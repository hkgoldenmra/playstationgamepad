import pyautogui
import serial
import sys

# Linux - /dev/tty*
# Windows - COM*
# Mac OS - /dev/cu.usb*
comPort = "/dev/ttyACM0"
baudRate = 115200
arduino = serial.Serial(comPort, baudRate)

# button map to keyboard keys
# buttons[0] = Select
# buttons[1] = L3 (Analog gamepad only)
# buttons[2] = R3 (Analog gamepad only)
# buttons[3] = Start
# buttons[4] = Up
# buttons[5] = Right
# buttons[6] = Down
# buttons[7] = Left
# buttons[8] = L2
# buttons[9] = R2
# buttons[10] = L1
# buttons[11] = R1
# buttons[12] = Triangle
# buttons[13] = Circle
# buttons[14] = Cross
# buttons[15] = Square
buttons = ["prtsc", "home", "end", "fn", "up", "right", "down", "left", "win", "shift", "alt", "ctrl", "space", "enter", "x", "z"]
keysDown = {}
#deadZone = 50
#speed = 1

def keyDown(key):
	if key not in keysDown:
		keysDown[key] = 1
		pyautogui.keyDown(key)

def keyUp(key):
	if key in keysDown:
		del(keysDown[key])
		pyautogui.keyUp(key)

try:
	print sys.argv[0], "is running ......"
	print "Press Ctrl-C to stop the daemon !"
	while 1:
		line = arduino.readline().strip()
		fields = line.split(",")
		if len(fields) >= 21:
#			print line
			for index, field in enumerate(fields):
				# 16 buttons
				if index >= 0 and index < 16:
					if field == "1":
						keyDown(buttons[index])
					else:
						keyUp(buttons[index])
#				# left analog
#				elif index >= 16 and index < 18:
#				if index >= 16 and index < 18:
#					width, height = pyautogui.size()
#					x, y = pyautogui.position()
#					value = int(field)
#					if index == 16:
#						if value < -deadZone:
#							pyautogui.move(-speed, 0)
#						elif value > deadZone:
#							pyautogui.move(speed, 0)
#					elif index == 17:
#						if value < -deadZone:
#							pyautogui.move(0, -speed)
#						elif value > deadZone:
#							pyautogui.move(0, speed)
#				# right analog
#				elif index >= 18 and index < 20:

# Press Ctrl-C
except KeyboardInterrupt:
	arduino.close()
	print
	print sys.argv[0], "is stopped !"
