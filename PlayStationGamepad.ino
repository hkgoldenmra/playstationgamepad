#include "PlayStationGamepad.h"

// Constructs a PlayStationGamepad instance.
// Pin 2 = Data Pin
// Pin 3 = Command Pin
// Pin 4 = Attention Pin
// Pin 5 = Clock Pin
PlayStationGamepad psg(2, 3, 4, 5);

void setup() {
	Serial.begin(115200);
	psg.initial();
}

void loop() {
	// Updates the signal from the connected PlayStationGamepad instance.
	// 1st parameter is boolean, true is enable small vibration motor, false is disable.
	// 2nd parameter is byte, large vibration motor level between 0x40 to 0xFF, 0x00 to 0x3F will not activate.
	psg.updateSignal(false, 0x00);
	String message = "";
	bool presseds[] = {
		psg.isSelectPressed(), psg.isL3Pressed(), psg.isR3Pressed(), psg.isStartPressed(),
		psg.isUpPressed(), psg.isRightPressed(), psg.isDownPressed(), psg.isLeftPressed(),
		psg.isL2Pressed(), psg.isR2Pressed(), psg.isL1Pressed(), psg.isR1Pressed(),
		psg.isTrianglePressed(), psg.isCirclePressed(), psg.isCrossPressed(), psg.isSquarePressed()
	};
	int analogs[] = {
		psg.getLeftX(), psg.getLeftY(), psg.getRightX(), psg.getRightY()
	};
	byte pressures[] = {
		psg.getUp(), psg.getRight(), psg.getDown(), psg.getLeft(),
		psg.getL2(), psg.getR2(), psg.getL1(), psg.getR1(),
		psg.getTriangle(), psg.getCircle(), psg.getCross(), psg.getSquare()
	};
	for (bool pressed : presseds) {
		char temp[4];
		sprintf(temp, "%u, ", (pressed ? 1 : 0));
		message += temp;
	}
	for (int analog : analogs) {
		char temp[7];
		sprintf(temp, "%+04d, ", analog);
		message += temp;
	}
	for (byte pressure : pressures) {
		char temp[6];
		sprintf(temp, "%03u, ", pressure);
		message += temp;
	}
	if (false) {
		char temp[8];
		sprintf(temp, "%05u, ", psg.getButtonSignal());
		message += temp;
	}
	Serial.println(message);

	// 60 fps ~ 0.016 seconds
	delay(16);
}