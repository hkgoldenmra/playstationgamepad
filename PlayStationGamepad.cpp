#include "PlayStationGamepad.h"

const byte PlayStationGamepad::COMMAND_CHAIN_TEMPLATE[21] = {
	PlayStationGamepad::COMMAND_HANDSHAKE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE,
	PlayStationGamepad::COMMAND_IDLE
};

/**
The PlayStationGamepad constructor.
@param byte datPin The Data pin object PlayStation gamepad.
@param byte cmdPin The Command pin object PlayStation gamepad.
@param byte attPin The Attention pin object PlayStation gamepad.
@param byte clkPin The Clock pin object PlayStation gamepad.
*/
PlayStationGamepad::PlayStationGamepad(byte datPin, byte cmdPin, byte attPin, byte clkPin) {
	this->datPin = datPin;
	this->cmdPin = cmdPin;
	this->attPin = attPin;
	this->clkPin = clkPin;
}

/*
Sets the command is opened or closed.
@param bool enabled open if true, close if close
*/
void PlayStationGamepad::setCommand(bool enabled) {
	// Sets the Attention Pin to 
	// LOW signal to start the handshake
	// HIGH signal to stop the handshake
	digitalWrite(this->attPin, ((enabled) ? LOW : HIGH));
	// delays the a byte signal before send the command
	delayMicroseconds(PlayStationGamepad::BYTE_DELAY);
}

/*
Sends a byte of Command signal to the gamepad.
@param byte command a 8-bit command
@return byte the Data signal after Command signal is sent
*/
byte PlayStationGamepad::sendCommand(byte command) {
	byte response = 0;
	// byte period
	for (byte i = 0; i < 8; i++) {
		// shifted a bit to 1, 2, 4, 8, 16, 32, 64, 128 in this period
		const byte SHIFTED = 1 << i;
		// sends LOW signal to the Clock Pin to send a signal to the Command Pin
		digitalWrite(this->clkPin, LOW);
		// checks if this bit is exist or not
		if ((command & SHIFTED) > 0) {
			// sends HIGH signal to the Command Pin if this bit is exist
			digitalWrite(this->cmdPin, HIGH);
		} else {
			// sends LOW signal to the Command Pin if this bit is not exist
			digitalWrite(this->cmdPin, LOW);
		}
		// delays the a bit signal
		delayMicroseconds(PlayStationGamepad::BIT_DELAY);
		// sends HIGH signal to the clock to received a signal from the Data Pin
		digitalWrite(this->clkPin, HIGH);
		// combines the response if Data signal is greater than LOW signal
		// LOW signal also is a signal, however, there is no different if response |= 0, so no need to combine
		if (digitalRead(this->datPin) > LOW) {
			response |= SHIFTED;
		}
		// delays the a bit signal
		delayMicroseconds(PlayStationGamepad::BIT_DELAY);
	}
	// delays the a byte signal
	delayMicroseconds(PlayStationGamepad::BYTE_DELAY);
	return response;
}

/*
Sends list of command signals.
@param byte[] commands list of command signals
@param byte LENGTH length of commands
*/
void PlayStationGamepad::sendCommands(byte commands[], byte LENGTH) {
	this->setCommand(true);
	for (byte i = 0; i < LENGTH; i++) {
		this->sendCommand(commands[i]);
	}
	this->setCommand(false);
}

/*
Sends list of command signals to setup pressure buttons.
@param bool enabled true to enable, false to disable
*/
void PlayStationGamepad::setPressureButtons(bool enabled) {
	byte commands[10];
	memcpy(commands, PlayStationGamepad::COMMAND_CHAIN_TEMPLATE, sizeof(commands));
	// Edits the operation command to the "Pressure Buttons" command
	commands[1] = PlayStationGamepad::COMMAND_PRESSURE_BUTTONS;
	// The 5th command controls the sensor which 0x00 is disable, 0x02 is enable
	commands[4] = (enabled ? 0x02 : 0x00);
	for (byte i = 0; i < 12; i++) {
		// The 4th command controls which button is affected
		// 0 - Right
		// 1 - Left
		// 2 - Up
		// 3 - Down
		// 4 - Triangle
		// 5 - Circle
		// 6 - Cross
		// 7 - Square
		// 8 - L1
		// 9 - R1
		// 10 - L2
		// 11 - R2
		commands[3] = i;
		this->sendCommands(commands, sizeof(commands) / sizeof(byte));
	}
}

/*
Sends list of command signals to open configuration.
@param bool enabled true to enable, false to disable
*/
void PlayStationGamepad::setConfig(bool enabled) {
	byte commands[10];
	// Copies the template command array to this command array
	memcpy(commands, PlayStationGamepad::COMMAND_CHAIN_TEMPLATE, sizeof(commands));
	// Edits the operation command to the "Config" command
	commands[1] = PlayStationGamepad::COMMAND_CONFIG;
	// The 4th command controls the configuration which 0x00 is disable, 0x01 is enable
	commands[3] = (enabled ? 0x01 : 0x00);
	this->sendCommands(commands, sizeof(commands) / sizeof(byte));
}

/*
Sends list of command signals to start analog.
@param bool enabled true to enable, false to disable
@param bool locked true to enable, false to disable
*/
void PlayStationGamepad::setAnalog(bool enabled, bool locked) {
	byte commands[10];
	memcpy(commands, PlayStationGamepad::COMMAND_CHAIN_TEMPLATE, sizeof(commands));
	// Edits the operation command to the "Analog" command
	commands[1] = PlayStationGamepad::COMMAND_ANALOG;
	// The 4th command controls the analog mode which 0x00 is disable (Digital mode), 0x01 is enable (Analog mode)
	commands[3] = (enabled ? 0x01 : 0x00);
	// The 5th command controls the mode which 0x00 is unlocked, 0x03 is locked
	commands[4] = (locked ? 0x03 : 0x00);
	this->sendCommands(commands, sizeof(commands) / sizeof(byte));
}

/*
Sends list of command signals to start analog.
@param bool enabled true to enable, false to disable
*/
void PlayStationGamepad::setAnalog(bool enabled) {
	this->setAnalog(enabled, false);
}

/*
Sends list of command signals to start vibration.
@param bool smallMotor true to enable small vibration motor, false to disable
@param bool largeMotor true to enable large vibration motor, false to disable
*/
void PlayStationGamepad::setVibration(bool smallMotor, bool largeMotor) {
	byte commands[10];
	memcpy(commands, PlayStationGamepad::COMMAND_CHAIN_TEMPLATE, sizeof(commands));
	// Edits the operation command to the "Vibration" command
	commands[1] = PlayStationGamepad::COMMAND_VIBRATION;
	// The 4th command controls the small vibration motor
	commands[3] = (smallMotor ? 0x00 : 0xFF);
	// The 5th command controls the large vibration motor
	commands[4] = (largeMotor ? 0x01 : 0xFF);
	commands[5] = 0xFF;
	commands[6] = 0xFF;
	commands[7] = 0xFF;
	commands[8] = 0xFF;
	commands[9] = 0xFF;
	this->sendCommands(commands, sizeof(commands) / sizeof(byte));
}

/*
Sends list of command signals to start pressure sensor.
@param bool enabled true to enable, false to disable
*/
void PlayStationGamepad::setPressureSensor(bool enabled) {
	byte commands[10];
	memcpy(commands, PlayStationGamepad::COMMAND_CHAIN_TEMPLATE, sizeof(commands));
	// Edits the operation command to the "Pressure Sensor" command
	commands[1] = PlayStationGamepad::COMMAND_PRESSURE_SENSOR;
	// The 5th command controls the sensor which 0x00 is disable, 0x03 is enable
	commands[3] = 0xFF;
	commands[4] = 0xFF;
	commands[5] = (enabled ? 0x03 : 0x00);
	this->sendCommands(commands, sizeof(commands) / sizeof(byte));
}

/*
Initials the configurations (Analog Gamepad requires).
*/
void PlayStationGamepad::initial() {
	pinMode(this->datPin, INPUT);
	pinMode(this->cmdPin, OUTPUT);
	pinMode(this->attPin, OUTPUT);
	pinMode(this->clkPin, OUTPUT);
	digitalWrite(this->datPin, HIGH);
	digitalWrite(this->cmdPin, LOW);
	digitalWrite(this->attPin, HIGH);
	digitalWrite(this->clkPin, HIGH);
	this->setConfig(true);
	this->setAnalog(true, true);
	this->setVibration(true, true);
	this->setPressureButtons(true);
	this->setPressureSensor(true);
	this->setConfig(false);
}

/*
Maps 0 ~ 255 to -100 ~ 100.
*/
int PlayStationGamepad::valueMap(byte value) {
	return map(value, 0x00, 0xFF, -100, 100);
}

/*
Return which signal of button is pressed.
@return bool
*/
bool PlayStationGamepad::isButtonPressed(unsigned int button) {
	return (this->getButtonSignal() & button) == 0;
}

/**
Updates the buttons and axes signals on PlayStation gamepad.
@param enabled bool start small vibration motor (?)
@level level byte large vibration motor level
*/
void PlayStationGamepad::updateSignal(bool enabled, byte level) {
	// connects the gamepad before send commands
	this->setCommand(true);

	// Sends the "Connect (0x01)" command
	// If success, returns 1st byte "Connected (0xFF)" response
	this->sendCommand(PlayStationGamepad::COMMAND_HANDSHAKE);

	// Sends the "Data (0x42)" command
	// If success, returns 2nd byte response which represents
	// 0x41 - Standard Gamepad
	// 0x73 - Analog Gamepad
	this->type = this->sendCommand(PlayStationGamepad::COMMAND_DATA);

	// Sends the "Idle (0x00)" command
	// If success, returns 3rd byte "Ready (0x5A)" response
	this->sendCommand(PlayStationGamepad::COMMAND_IDLE);

	// Standard Gamepad
	// 4th command controls small vibration motor activate or not
	// 4th response byte signal are Select, L3, R3, Start, Up, Right, Down, Left
	this->digitalSignals[0] = this->sendCommand(enabled ? 0xFF : 0x00);
	// 5th command controls large vibration motor speed between 0x40 to 0xFF
	// 5th response byte signal are L2, R2, L1, R1, Triangle, Circle, Cross, Square
	this->digitalSignals[1] = this->sendCommand(level);

	byte LENGTH;
	// Analog Gamepad (Analog enabled)
	// 6th byte signal is the Right Analog X-Axis value
	// 7th byte signal is the Right Analog Y-Axis value
	// 8th byte signal is the Left Analog X-Axis value
	// 9th byte signal is the Left Analog Y-Axis value
	// X-Axis value represents
	// 0x00 - Left
	// 0xFF - Right
	// Y-Axis value represents
	// 0x00 - Top
	// 0xFF - Bottom
	LENGTH = sizeof(this->analogSignals) / sizeof(byte);
	for (byte i = 0; i < LENGTH; i++) {
		this->analogSignals[i] = this->sendCommand(PlayStationGamepad::COMMAND_IDLE);
	}

	// DualShock2 Gamepad (Analog enabled)
	// 10th byte signal is the pressure of Right value
	// 11th byte signal is the pressure of Left value
	// 12th byte signal is the pressure of Up value
	// 13th byte signal is the pressure of Down value
	// 14th byte signal is the pressure of Triangle value
	// 15th byte signal is the pressure of Circle value
	// 16th byte signal is the pressure of Cross value
	// 17th byte signal is the pressure of Square value
	// 18th byte signal is the pressure of L1 value
	// 19th byte signal is the pressure of R1 value
	// 20th byte signal is the pressure of L2 value
	// 21st byte signal is the pressure of R2 value
	LENGTH = sizeof(this->pressureSignals) / sizeof(byte);
	for (byte i = 0; i < LENGTH; i++) {
		this->pressureSignals[i] = this->sendCommand(PlayStationGamepad::COMMAND_IDLE);
	}

	// disconnects the gamepad after finish
	// you must disconnect the gamepad if a period is completed
	this->setCommand(false);
}

void PlayStationGamepad::updateSignal() {
	this->updateSignal(false, 0x00);
}

/**
Returns the gamepad type from a signal.
@return byte
*/
byte PlayStationGamepad::getType() {
	return this->type;
}

/**
Returns the gamepad name from the type.
@return String
*/
String PlayStationGamepad::getName() {
	// add more types here if exist
	if (this->getType() == PlayStationGamepad::GAMEPAD_STANDARD) {
		return "Standard";
	} else if (this->getType() == PlayStationGamepad::GAMEPAD_ANALOG) {
		return "Analog";
	} else {
		return "Unknown";
	}
}

/**
Returns the button signal.
@return unsigned int
*/
unsigned int PlayStationGamepad::getButtonSignal() {
	return this->digitalSignals[0] | (this->digitalSignals[1] << 8);
}

/**
Return true if Select button is pressed.
@return bool
*/
bool PlayStationGamepad::isSelectPressed() {
	return this->isButtonPressed(PlayStationGamepad::SELECT);
}

/**
Return true if L3 button is pressed.
@return bool
*/
bool PlayStationGamepad::isL3Pressed() {
	return this->isButtonPressed(PlayStationGamepad::L3);
}

/**
Return true if R3 button is pressed.
@return bool
*/
bool PlayStationGamepad::isR3Pressed() {
	return this->isButtonPressed(PlayStationGamepad::R3);
}

/**
Return true if Start button is pressed.
@return bool
*/
bool PlayStationGamepad::isStartPressed() {
	return this->isButtonPressed(PlayStationGamepad::START);
}

/**
Return true if Up button is pressed.
@return bool
*/
bool PlayStationGamepad::isUpPressed() {
	return this->isButtonPressed(PlayStationGamepad::UP);
}

/**
Return true if Right button is pressed.
@return bool
*/
bool PlayStationGamepad::isRightPressed() {
	return this->isButtonPressed(PlayStationGamepad::RIGHT);
}

/**
Return true if Down button is pressed.
@return bool
*/
bool PlayStationGamepad::isDownPressed() {
	return this->isButtonPressed(PlayStationGamepad::DOWN);
}

/**
Return true if Left button is pressed.
@return bool
*/
bool PlayStationGamepad::isLeftPressed() {
	return this->isButtonPressed(PlayStationGamepad::LEFT);
}

/**
Return true if L2 button is pressed.
@return bool
*/
bool PlayStationGamepad::isL2Pressed() {
	return this->isButtonPressed(PlayStationGamepad::L2);
}

/**
Return true if R2 button is pressed.
@return bool
*/
bool PlayStationGamepad::isR2Pressed() {
	return this->isButtonPressed(PlayStationGamepad::R2);
}

/**
Return true if L1 button is pressed.
@return bool
*/
bool PlayStationGamepad::isL1Pressed() {
	return this->isButtonPressed(PlayStationGamepad::L1);
}

/**
Return true if R1 button is pressed.
@return bool
*/
bool PlayStationGamepad::isR1Pressed() {
	return this->isButtonPressed(PlayStationGamepad::R1);
}

/**
Return true if Triangle button is pressed.
@return bool
*/
bool PlayStationGamepad::isTrianglePressed() {
	return this->isButtonPressed(PlayStationGamepad::TRIANGLE);
}

/**
Return true if Circle button is pressed.
@return bool
*/
bool PlayStationGamepad::isCirclePressed() {
	return this->isButtonPressed(PlayStationGamepad::CIRCLE);
}

/**
Return true if Cross button is pressed.
@return bool
*/
bool PlayStationGamepad::isCrossPressed() {
	return this->isButtonPressed(PlayStationGamepad::CROSS);
}

/**
Return true if Square button is pressed.
@return bool
*/
bool PlayStationGamepad::isSquarePressed() {
	return this->isButtonPressed(PlayStationGamepad::SQUARE);
}

/**
Returns the Right Analog Stick X-Axis value which between -100 and 100.
Negative to be left.
Positive to be right.
@return int
*/
int PlayStationGamepad::getRightX() {
	return this->valueMap(this->analogSignals[0]);
}

/**
Returns the Right Analog Stick Y-Axis value which between -100 and 100.
Negative to be top.
Positive to be bottom.
@return int
*/
int PlayStationGamepad::getRightY() {
	return this->valueMap(this->analogSignals[1]);
}

/**
Returns the Left Analog Stick X-Axis value which between -100 and 100.
Negative to be left.
Positive to be right.
@return int
*/
int PlayStationGamepad::getLeftX() {
	return this->valueMap(this->analogSignals[2]);
}

/**
Returns the Left Analog Stick Y-Axis value which between -100 and 100.
Negative to be top.
Positive to be bottom.
@return int
*/
int PlayStationGamepad::getLeftY() {
	return this->valueMap(this->analogSignals[3]);
}

/**
Returns the Up value which between 0 and 255.
0 to be unpress.
255 to be press completely.
@return byte
*/
byte PlayStationGamepad::getUp() {
	return this->pressureSignals[2];
}

/**
Returns the Up value which between 0 and 255.
0 to be unpress.
255 to be press completely.
@return byte
*/
byte PlayStationGamepad::getRight() {
	return this->pressureSignals[0];
}

/**
Returns the Down value which between 0 and 255.
0 to be unpress.
255 to be press completely.
@return byte
*/
byte PlayStationGamepad::getDown() {
	return this->pressureSignals[3];
}

/**
Returns the Left value which between 0 and 255.
0 to be unpress.
255 to be press completely.
@return byte
*/
byte PlayStationGamepad::getLeft() {
	return this->pressureSignals[1];
}

/**
Returns the L2 value which between 0 and 255.
0 to be unpress.
255 to be press completely.
@return byte
*/
byte PlayStationGamepad::getL2() {
	return this->pressureSignals[10];
}

/**
Returns the R2 value which between 0 and 255.
0 to be unpress.
255 to be press completely.
@return byte
*/
byte PlayStationGamepad::getR2() {
	return this->pressureSignals[11];
}

/**
Returns the L1 value which between 0 and 255.
0 to be unpress.
255 to be press completely.
@return byte
*/
byte PlayStationGamepad::getL1() {
	return this->pressureSignals[8];
}

/**
Returns the R1 value which between 0 and 255.
0 to be unpress.
255 to be press completely.
@return byte
*/
byte PlayStationGamepad::getR1() {
	return this->pressureSignals[9];
}

/**
Returns the Triangle value which between 0 and 255.
0 to be unpress.
255 to be press completely.
@return byte
*/
byte PlayStationGamepad::getTriangle() {
	return this->pressureSignals[4];
}

/**
Returns the Circle value which between 0 and 255.
0 to be unpress.
255 to be press completely.
@return byte
*/
byte PlayStationGamepad::getCircle() {
	return this->pressureSignals[5];
}

/**
Returns the Cross value which between 0 and 255.
0 to be unpress.
255 to be press completely.
@return byte
*/
byte PlayStationGamepad::getCross() {
	return this->pressureSignals[6];
}

/**
Returns the Square value which between 0 and 255.
0 to be unpress.
255 to be press completely.
@return byte
*/
byte PlayStationGamepad::getSquare() {
	return this->pressureSignals[7];
}