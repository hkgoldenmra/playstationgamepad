#include <Arduino.h>

class PlayStationGamepad {
	private:
		// object attributes
		byte datPin;
		byte cmdPin;
		byte attPin;
		byte clkPin;
		byte type;
		byte digitalSignals[2];
		byte analogSignals[4];
		byte pressureSignals[12];

		// object methods
		void setCommand(bool);
		byte sendCommand(byte);
		void sendCommands(byte[], const byte LENGTH);
		void setPressureButtons(bool);
		void setConfig(bool);
		void setAnalog(bool, bool);
		void setAnalog(bool);
		void setVibration(bool, bool);
		void setPressureSensor(bool);
		int valueMap(byte);
		bool isButtonPressed(unsigned int);
	public:
		// class attributes

		// delay constants
		/// The delay in mircoseconds between bits
		static const byte BIT_DELAY = 0x02;

		/// The delay in mircoseconds between bytes
		static const byte BYTE_DELAY = PlayStationGamepad::BIT_DELAY * 8;

		// comamnd constants
		/// The "Idle" command
		static const byte COMMAND_IDLE = 0x00;

		/// The "Handshake" command
		static const byte COMMAND_HANDSHAKE = 0x01;

		/// The "Pressure Buttons" command
		static const byte COMMAND_PRESSURE_BUTTONS = 0x40;

		/// The "Data" command
		static const byte COMMAND_DATA = 0x42;

		/// The "Config" command
		static const byte COMMAND_CONFIG = 0x43;

		/// The "Analog" command
		static const byte COMMAND_ANALOG = 0x44;

		/// The "Vibration" command
		static const byte COMMAND_VIBRATION = 0x4D;

		/// The "Pressure Sensor" command
		static const byte COMMAND_PRESSURE_SENSOR = 0x4F;

		/// A command chain template for operations
		static const byte COMMAND_CHAIN_TEMPLATE[21];

		// response constants
		/// The "Connected" response
		static const byte RESPONSE_CONNECTED = 0xFF;

		/// The "Ready" response
		static const byte RESPONSE_READY = 0x5A;

		// gamepad type constants
		/// The Standard Gamepad ID
		static const byte GAMEPAD_STANDARD = 0x41;

		/// The Analog Gamepad ID
		static const byte GAMEPAD_ANALOG = 0x73;

		// button constants
		/// The PlayStation gamepad Select signal
		static const unsigned int SELECT = 1 << 0;

		/// The PlayStation gamepad L3 signal (Analog Gamepad only)
		static const unsigned int L3 = 1 << 1;

		/// The PlayStation gamepad R3 signal (Analog Gamepad only)
		static const unsigned int R3 = 1 << 2;

		/// The PlayStation gamepad Start signal
		static const unsigned int START = 1 << 3;

		/// The PlayStation gamepad D-Pad Up signal
		static const unsigned int UP = 1 << 4;

		/// The PlayStation gamepad D-Pad Right signal
		static const unsigned int RIGHT = 1 << 5;

		/// The PlayStation gamepad D-Pad Down signal
		static const unsigned int DOWN = 1 << 6;

		/// The PlayStation gamepad D-Pad Left signal
		static const unsigned int LEFT = 1 << 7;

		/// The PlayStation gamepad L2 signal
		static const unsigned int L2 = PlayStationGamepad::SELECT << 8;

		/// The PlayStation gamepad R2 signal
		static const unsigned int R2 = PlayStationGamepad::L3 << 8;

		/// The PlayStation gamepad L1 signal
		static const unsigned int L1 = PlayStationGamepad::R3 << 8;

		/// The PlayStation gamepad R1 signal
		static const unsigned int R1 = PlayStationGamepad::START << 8;

		/// The PlayStation gamepad Triangle signal
		static const unsigned int TRIANGLE = PlayStationGamepad::UP << 8;

		/// The PlayStation gamepad Circle signal
		static const unsigned int CIRCLE = PlayStationGamepad::RIGHT << 8;

		/// The PlayStation gamepad Cross signal
		static const unsigned int CROSS = PlayStationGamepad::DOWN << 8;

		/// The PlayStation gamepad Square signal
		static const unsigned int SQUARE = PlayStationGamepad::LEFT << 8;

		// constructor
		PlayStationGamepad(byte, byte, byte, byte);

		// object methods
		void initial();
		void updateSignal(bool, byte);
		void updateSignal();
		byte getType();
		String getName();
		unsigned int getButtonSignal();
		bool isSelectPressed();
		bool isL3Pressed();
		bool isR3Pressed();
		bool isStartPressed();
		bool isUpPressed();
		bool isRightPressed();
		bool isDownPressed();
		bool isLeftPressed();
		bool isL2Pressed();
		bool isR2Pressed();
		bool isL1Pressed();
		bool isR1Pressed();
		bool isTrianglePressed();
		bool isCirclePressed();
		bool isCrossPressed();
		bool isSquarePressed();
		int getRightX();
		int getRightY();
		int getLeftX();
		int getLeftY();
		byte getUp();
		byte getRight();
		byte getDown();
		byte getLeft();
		byte getL2();
		byte getR2();
		byte getL1();
		byte getR1();
		byte getTriangle();
		byte getCircle();
		byte getCross();
		byte getSquare();
};