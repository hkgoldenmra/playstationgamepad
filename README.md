# Abstract
Just for fun and challenging.
# Diagram
```
  1 2 3   4 5 6   7 8 9
|-----------------------|
| . . . | . . . | . . . |
@-----------------------@
```
* @ is the "Error Prevention" side
# Attribute
* **Pin 1 - Brown - Input - Data (Required)**
* **Pin 2 - Orange - Output - Command (Required)**
* Pin 3 - Grey - None - 7.0V to 9.0V for Vibration Motor (Dual Shock only) (Optional)
* **Pin 4 - Black - None - Ground (Required)**
* **Pin 5 - Red - None - 3.3V to 5.0V for Power (Required)**
* **Pin 6 - Yellow - Output - Attention (Required)**
* **Pin 7 - Blue - Output - Clock (Required)**
* Pin 8 - White - None - Not in use (Optional)
* Pin 9 - Green - Input - Acknowledge (Optional)
# Usage
* Data
	* Gamepad sends the signal to the target which represent the button press and analog.
* Command
	* Target sends the signal to the gamepad, can be initial configuration such as analog enabling, button status.
* Attention
	* LOW signal is ready to send packet to the gamepad.
	* HIGH signal is complete to send packet to the gamepad.
* Clock
	* LOW signal is ready to send commands to the gamepad.
	* HIGH signal is ready to receive responses from the gamepad.
* Acknowledge
	* Acknowledge no need to connect to the gamepad.
# Signal
```
|----------------------------------------------------------------------------|
|          |          |Command and Response period                           |
|----------------------------------------------------------------------------|
|          |          |          |byte period          |          |          |
|----------------------------------------------------------------------------|
|Pin       |Initial   |Start     |CMD bit   |RES bit   |Complete  |Stop      |
|----------------------------------------------------------------------------|
|att       |----------|___________________________________________|----------|
|----------------------------------------------------------------------------|
|clk       |----------|          |__________|----------|          |          |
|----------------------------------------------------------------------------|
|cmd       |__________|          |??????????|          |          |          |
|----------------------------------------------------------------------------|
|dat       |----------|          |          |??????????|          |          |
|----------------------------------------------------------------------------|
|Time      |          |delay 16us|delay 2us |delay 2us |delay 16us|delay 16us|
|----------------------------------------------------------------------------|
```
* **Gamepad Signal** = Intital + Command and Response Period * n
* **Command and Response period** = cmd start + byte period + cmd stop
* **byte period** = loops 8 times in bit
* **cmd start** = att sets to LOW signal
* **cmd stop** = att sets to HIGH signal
* **----------** = HIGH signal
* **__________** = LOW signal
* **??????????** = the signal can be HIGH or LOW
* **delay 16us** = the signal will delay in 16 microseconds
# How to build
## PlayStation gamepad
* Connects
	* **PlayStation gamepad Pin 1** and **Arduino Digital Pin 2**
	* **PlayStation gamepad Pin 2** and **Arduino Digital Pin 3**
	* **PlayStation gamepad Pin 3** and **Arduino V5.0 Pin**
	* **PlayStation gamepad Pin 4** and **Arduino GND Pin**
	* **PlayStation gamepad Pin 5** and **Arduino V3.3 Pin**
	* **PlayStation gamepad Pin 6** and **Arduino Digital Pin 4**
	* **PlayStation gamepad Pin 7** and **Arduino Digital Pin 5**
	* Remark
		* No pins on Arduino are specify to use.
		* **Digital Pin 2 to 13** and **Analog Pin A0 to A5** can be used.
## Arduino
1. Includes the library **PlayStationGamepad.h**
2. Constructs a PlayStationGamepad instance
3. Calls updateSignal method of PlayStationGamepad instance
4. Buttons and Axes
	* Buttons
		* Calls getButtonSignal method to get an unsigned int button signal
			* Signals
				* 0x0001 - Select
				* 0x0002 - L3
				* 0x0004 - R3
				* 0x0008 - Start
				* 0x0010 - Up
				* 0x0020 - Right
				* 0x0040 - Down
				* 0x0080 - Left
				* 0x0100 - L2
				* 0x0200 - R2
				* 0x0400 - L1
				* 0x0800 - R1
				* 0x1000 - Triangle
				* 0x2000 - Circle
				* 0x4000 - Cross
				* 0x8000 - Square
		* Or calls is*Pressed method to get a boolean to check the corresponding button is pressed or not
	* Axes
		* Calls get{Left,Right}{X,Y} method to get an int of corresponding axis value
			* The value is mapped between -100 and 100
			* X-Axis
				* Negative to be left
				* Positive to be right
			* Y-Axis
				* Negative to be top
				* Positive to be bottom
5. Basic Code
	* Includes header file "PlayStationGamepad.h"
	* Declares a PlayStationGamepad object with 4 parameters in global area
		* 1st parameter - Data Pin
		* 2nd parameter - Command Pin
		* 3rd parameter - Attention Pin
		* 4th parameter - Clock Pin
	* run initial() method of PlayStationGamepad object in void setup(){}
	* run updateSignal() method of PlayStationGamepad object in void loop(){}
```
#include "PlayStationGamepad.h"
PlayStationGamepad psg(2, 3, 4, 5);
void setup() {
    Serial.begin(115200);
    psg.initial();
}
void loop() {
    psg.updateSignal(false, 0x00);
    // do something
    delay(16);
}
```
## Python
* Executes the **PlayStationGamepadDaemon.py** to start the **Convertion Daemon**.
* Receive the signal from the Serial of Arduino.
* Converts the signal to simulate the keyboard and mouse keys.
# Flow
## Command and Response
```
|------------------------------------------------------|
|nth byte  |cmd hex   |Command   |dat hex   |Response  |
|----------|----------|----------|----------|----------|
|1         |0x01      |Handshake |0xFF      |Connected |
|----------|----------|----------|----------|----------|
|2         |0x??      |Operation |0x??      |Gamepad ID|
|----------|----------|----------|----------|----------|
|3         |0x00      |Idle      |0x5A      |Ready     |
|------------------------------------------------------|
```
* Each complete operation requires 3 bytes of command.
## Operation
* 0x40 - Pressure Buttons
	* Set which button will be pressure button.
* 0x42 - Data
	* Response buttons and axes signals.
* 0x43 - Configuration
	* Open or close configuration to enable or disable Analog, Vibration, Pressure Buttons, Pressure Sensor.
* 0x44 - Analog
	* Enable or disable analog.
* 0x4D - Vibration
	* Enable or disable vibration.
* 0x4F - Pressure Sensor
	* Enable or disable pressure sensor.
### Configuration
```
|------------------------------------------------------|
|nth byte  |cmd hex   |Command   |dat hex   |Response  |
|----------|----------|----------|----------|----------|
|1         |0x01      |Handshake |0xFF      |Connected |
|----------|----------|----------|----------|----------|
|2         |0x43      |Config    |0x??      |Gamepad ID|
|----------|----------|----------|----------|----------|
|3         |0x00      |Idle      |0x5A      |Ready     |
|----------|----------|----------|----------|----------|
|4         |0x??      |Boolean   |0xFF      |          |
|----------|----------|----------|----------|----------|
|5         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|6         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|7         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|8         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|9         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|10        |0x00      |Idle      |0xFF      |          |
|------------------------------------------------------|
```
* The 4th byte is a Boolean to enable or disable the configuration mode.
	* 0x00 - disable
	* 0x01 - enable
### Analog
```
|------------------------------------------------------|
|nth byte  |cmd hex   |Command   |dat hex   |Response  |
|----------|----------|----------|----------|----------|
|1         |0x01      |Handshake |0xFF      |Connected |
|----------|----------|----------|----------|----------|
|2         |0x44      |Analog    |0x??      |Gamepad ID|
|----------|----------|----------|----------|----------|
|3         |0x00      |Idle      |0x5A      |Ready     |
|----------|----------|----------|----------|----------|
|4         |0x??      |Boolean   |0xFF      |          |
|----------|----------|----------|----------|----------|
|5         |0x??      |Boolean   |0xFF      |          |
|----------|----------|----------|----------|----------|
|6         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|7         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|8         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|9         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|10        |0x00      |Idle      |0xFF      |          |
|------------------------------------------------------|
```
* The 4th byte is a Boolean to enable or disable the analog mode.
	* 0x00 - disable
	* 0x01 - enable
* The 5th byte is a Boolean to enable or disable the mode locking.
	* 0x00 - disable
	* 0x03 - enable
### Vibration
```
|------------------------------------------------------|
|nth byte  |cmd hex   |Command   |dat hex   |Response  |
|----------|----------|----------|----------|----------|
|1         |0x01      |Handshake |0xFF      |Connected |
|----------|----------|----------|----------|----------|
|2         |0x40      |Analog    |0x??      |Gamepad ID|
|----------|----------|----------|----------|----------|
|3         |0x00      |Idle      |0x5A      |Ready     |
|----------|----------|----------|----------|----------|
|4         |0x??      |Boolean   |0xFF      |          |
|----------|----------|----------|----------|----------|
|5         |0x??      |Boolean   |0xFF      |          |
|----------|----------|----------|----------|----------|
|6         |0xFF      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|7         |0xFF      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|8         |0xFF      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|9         |0xFF      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|10        |0xFF      |Idle      |0xFF      |          |
|------------------------------------------------------|
```
* The 4th byte is a Boolean to enable or disable the small vibration motor.
	* 0xFF - disable
	* 0x00 - enable
* The 5th byte is a Boolean to enable or disable the large vibration motor.
	* 0xFF - disable
	* 0x01 - enable
### Pressure Buttons
```
|------------------------------------------------------|
|nth byte  |cmd hex   |Command   |dat hex   |Response  |
|----------|----------|----------|----------|----------|
|1         |0x01      |Handshake |0xFF      |Connected |
|----------|----------|----------|----------|----------|
|2         |0x40      |Analog    |0x??      |Gamepad ID|
|----------|----------|----------|----------|----------|
|3         |0x00      |Idle      |0x5A      |Ready     |
|----------|----------|----------|----------|----------|
|4         |0x??      |Button    |0xFF      |          |
|----------|----------|----------|----------|----------|
|5         |0x??      |Boolean   |0xFF      |          |
|----------|----------|----------|----------|----------|
|6         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|7         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|8         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|9         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|10        |0x00      |Idle      |0xFF      |          |
|------------------------------------------------------|
```
* The 4th byte is a which button to be pressure button.
	* 0x00 - Right
	* 0x01 - Left
	* 0x02 - Up
	* 0x03 - Down
	* 0x04 - Triangle
	* 0x05 - Circle
	* 0x06 - Cross
	* 0x07 - Square
	* 0x08 - L1
	* 0x09 - R1
	* 0x0A - L2
	* 0x0B - R2
* The 5th byte is a Boolean to enable or disable the pressure button.
	* 0x00 - disable
	* 0x02 - enable
### Pressure Sensor
```
|------------------------------------------------------|
|nth byte  |cmd hex   |Command   |dat hex   |Response  |
|----------|----------|----------|----------|----------|
|1         |0x01      |Handshake |0xFF      |Connected |
|----------|----------|----------|----------|----------|
|2         |0x4F      |Analog    |0x??      |Gamepad ID|
|----------|----------|----------|----------|----------|
|3         |0x00      |Idle      |0x5A      |Ready     |
|----------|----------|----------|----------|----------|
|4         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|5         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|6         |0x??      |Boolean   |0xFF      |          |
|----------|----------|----------|----------|----------|
|7         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|8         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|9         |0x00      |Idle      |0xFF      |          |
|----------|----------|----------|----------|----------|
|10        |0x00      |Idle      |0xFF      |          |
|------------------------------------------------------|
```
* The 6th byte is a Boolean to enable or disable the pressure sensor.
	* 0x00 - disable
	* 0x03 - enable
* To enable the pressure of buttons, "Pressure Sensor" must follows "Pressure Buttons".
### Standard Gamepad
```
|-------------------------------------------------------------------------------------------------------------------|
|          |          |          |          |Response                                                               |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|nth byte  |cmd hex   |Command   |dat hex   |0th bit |1st bit |2nd bit |3rd bit |4th bit |5th bit |6th bit |7th bit |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|1         |0x01      |Handshake |0xFF      |Connected                                                              |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|2         |0x42      |Data      |0x41      |ID of Standard Gamepad                                                 |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|3         |0x00      |Idle      |0x5A      |Ready                                                                  |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|4         |0x00      |Idle      |0x??      |Select  |        |        |Start   |Up      |Right   |Down    |Left    |
|----------|----------|----------|----------|--------|--------|--------|--------|--------|--------|--------|--------|
|5         |0x00      |Idle      |0x??      |L2      |R2      |L1      |R1      |Triangle|Circle  |Cross   |Square  |
|-------------------------------------------------------------------------------------------------------------------|
```
* Standard Gamepad does not have L3, R3, the corresponding signals will always response 1.
### Analog Gamepad
```
|-------------------------------------------------------------------------------------------------------------------|
|          |          |          |          |Response                                                               |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|nth byte  |cmd hex   |Command   |dat hex   |0th bit |1st bit |2nd bit |3rd bit |4th bit |5th bit |6th bit |7th bit |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|1         |0x01      |Handshake |0xFF      |Connected                                                              |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|2         |0x42      |Data      |0x73      |ID of Analog Gamepad                                                   |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|3         |0x00      |Idle      |0x5A      |Ready                                                                  |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|4         |0x??      |S.Motor   |0x??      |Select  |L3      |R3      |Start   |Up      |Right   |Down    |Left    |
|----------|----------|----------|----------|--------|--------|--------|--------|--------|--------|--------|--------|
|5         |0x??      |L.Motor   |0x??      |L2      |R2      |L1      |R1      |Triangle|Circle  |Cross   |Square  |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|6         |0x00      |Idle      |0x??      |Right Analog X-Axis Value - 0x00 = Left; 0xFF = Right                  |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|7         |0x00      |Idle      |0x??      |Right Analog Y-Axis Value - 0x00 = Top; 0xFF = Bottom                  |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|8         |0x00      |Idle      |0x??      |Left Analog X-Axis Value - 0x00 = Left; 0xFF = Right                   |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|9         |0x00      |Idle      |0x??      |Left Analog Y-Axis Value - 0x00 = Top; 0xFF = Bottom                   |
|-------------------------------------------------------------------------------------------------------------------|
```
* S.Motor which is enable or disable the small vibration motor.
	* 0x00 small vibration motor disable.
	* 0xFF small vibration motor enable.
	* Small vibratioin motor cannot change the level of vibration.
* L.Motor which is the vibration level of the large vibration motor.
	* The level between 0x40 to 0xFF.
	* 0x00 to 0x3F will not activate the large vibration motor.
* When Analog Mode not activate.
	* L3, R3 signals will always response 1.
	* Analog Axes signals will always response 0xFF.
### DualShock2 Gamepad
```
|-------------------------------------------------------------------------------------------------------------------|
|          |          |          |          |Response                                                               |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|nth byte  |cmd hex   |Command   |dat hex   |0th bit |1st bit |2nd bit |3rd bit |4th bit |5th bit |6th bit |7th bit |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|1         |0x01      |Handshake |0xFF      |Connected                                                              |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|2         |0x42      |Data      |0x73      |ID of Analog Gamepad                                                   |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|3         |0x00      |Idle      |0x5A      |Ready                                                                  |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|4         |0x??      |S.Motor   |0x??      |Select  |L3      |R3      |Start   |Up      |Right   |Down    |Left    |
|----------|----------|----------|----------|--------|--------|--------|--------|--------|--------|--------|--------|
|5         |0x??      |L.Motor   |0x??      |L2      |R2      |L1      |R1      |Triangle|Circle  |Cross   |Square  |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|6         |0x00      |Idle      |0x??      |Right Analog X-Axis Value - 0x00 = Left; 0xFF = Right                  |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|7         |0x00      |Idle      |0x??      |Right Analog Y-Axis Value - 0x00 = Top; 0xFF = Bottom                  |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|8         |0x00      |Idle      |0x??      |Left Analog X-Axis Value - 0x00 = Left; 0xFF = Right                   |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|9         |0x00      |Idle      |0x??      |Left Analog Y-Axis Value - 0x00 = Top; 0xFF = Bottom                   |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|10        |0x00      |Idle      |0x??      |Right Pressure Value - 0x00 = Unpress; 0xFF = Press Completely         |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|11        |0x00      |Idle      |0x??      |Left Pressure Value - 0x00 = Unpress; 0xFF = Press Completely          |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|12        |0x00      |Idle      |0x??      |Up Pressure Value - 0x00 = Unpress; 0xFF = Press Completely            |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|13        |0x00      |Idle      |0x??      |Down Pressure Value - 0x00 = Unpress; 0xFF = Press Completely          |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|14        |0x00      |Idle      |0x??      |Triangle Pressure Value - 0x00 = Unpress; 0xFF = Press Completely      |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|15        |0x00      |Idle      |0x??      |Circle Pressure Value - 0x00 = Unpress; 0xFF = Press Completely        |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|16        |0x00      |Idle      |0x??      |Cross Pressure Value - 0x00 = Unpress; 0xFF = Press Completely         |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|17        |0x00      |Idle      |0x??      |Square Pressure Value - 0x00 = Unpress; 0xFF = Press Completely        |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|18        |0x00      |Idle      |0x??      |L1 Pressure Value - 0x00 = Unpress; 0xFF = Press Completely            |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|19        |0x00      |Idle      |0x??      |R1 Pressure Value - 0x00 = Unpress; 0xFF = Press Completely            |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|20        |0x00      |Idle      |0x??      |L2 Pressure Value - 0x00 = Unpress; 0xFF = Press Completely            |
|----------|----------|----------|----------|-----------------------------------------------------------------------|
|21        |0x00      |Idle      |0x??      |R2 Pressure Value - 0x00 = Unpress; 0xFF = Press Completely            |
|-------------------------------------------------------------------------------------------------------------------|
```
* S.Motor which is enable or disable the small vibration motor.
	* 0x00 small vibration motor disable.
	* 0xFF small vibration motor enable.
	* Small vibratioin motor cannot change the level of vibration.
* L.Motor which is the vibration level of the large vibration motor.
	* The level between 0x40 to 0xFF.
	* 0x00 to 0x3F will not activate the large vibration motor.
* When Analog Mode not activate.
	* L3, R3 signals will always response 1.
	* Analog Axes signals will always response 0xFF.
	* Pressure Value signals will always response 0xFF.
# Credit and Reference
* This project is redistributed from [PSX Library @ playground.arduino.cc](https://playground.arduino.cc/Main/PSXLibrary "PSX Library @ playground.arduino.cc").
* [https://github.com/madsci1016/Arduino-PS2X/tree/master/PS2X_lib](https://github.com/madsci1016/Arduino-PS2X/tree/master/PS2X_lib "https://github.com/madsci1016/Arduino-PS2X/tree/master/PS2X_lib") which made by [Bill Porter](http://www.billporter.info/2010/06/05/playstation-2-controller-arduino-library-v1-0 "Bill Porter").
* [https://gamesx.com/controldata/psxcont/psxcont.htm](https://gamesx.com/controldata/psxcont/psxcont.htm "https://gamesx.com/controldata/psxcont/psxcont.htm").
* [http://kaele.com/~kashima/games/ps_jpn.txt](http://kaele.com/~kashima/games/ps_jpn.txt "http://kaele.com/~kashima/games/ps_jpn.txt") (Japenese, use "Shift_JIS" character set).
* [http://store.curiousinventor.com/guides/PS2](http://store.curiousinventor.com/guides/PS2 "http://store.curiousinventor.com/guides/PS2").
* [http://www.wegmuller.org/arduino/Arduino-Playstation_gameport.html](http://www.wegmuller.org/arduino/Arduino-Playstation_gameport.html "http://www.wegmuller.org/arduino/Arduino-Playstation_gameport.html").
* [https://gist.github.com/scanlime/5042071](https://gist.github.com/scanlime/5042071 "https://gist.github.com/scanlime/5042071").
* [https://github.com/AZO234/Arduino_PSXPad](https://github.com/AZO234/Arduino_PSXPad "https://github.com/AZO234/Arduino_PSXPad").
# Update
* 2019-06-29
	* The small and large vibration motor test complete
	* Update .h, .cpp, .ino, .fz, .svg, png file
	* Update README.md
* 2019-06-27
	* Add vibration settings
	* Update .fz and .svg file
	* Generate PlayStationGamepad.png file
	* Update README.md
* 2019-06-23
	* Add PlayStationGamepad.fz, PlayStationGamepad.svg
	* Update README.md
* 2019-06-20
	* Update button pressure value sensor
* 2019-06-19
	* Update compatibility between PlayStation One gamepad and PlayStation2 DualShock2 gamepad
* 2019-06-17
	* Individual the initial method out of the constructor
	* Update class methods
	* Update and refactor
	* Add comments
	* Delay 16ms the loop in PlayStationGamepad.ino file
* 2019-06-15
	* Update header and class support PlayStation DualShock gamepad and PlayStation2 DualShock2 gamepad
* 2019-06-11
	* Update PlayStationGamepad.h, PlayStationGamepad.cpp, PlayStationGamepad.ino, PlayStationGamepadDaemon.py
* 2019-06-08
	* Update README.md
* 2019-06-07
	* Add support method
	* Add comments
	* Update README.md
* 2019-06-06
	* Update to basic state when retrieve data
* 2019-06-05
	* Update README.md
* 2019-06-04
	* Add standard gamepad
	* Update README.md
* 2019-06-03
	* Add constant values and templates to PlayStationGamepad.h
	* Update updateSignal method in PlayStationGamepad.cpp
	* Update PlayStationGamepad.ino demo program
	* Update PlayStationGamepadDaemon.py
	* Update README.md
* 2019-05-31
	* Add comments
